﻿namespace Domain.SimpleEntities
{
    /// <summary>
    /// Идентификатор.
    /// </summary>
    public class Id : SimpleEntity<int>
    {
        /// <summary>
        /// Создать идентификатор.
        /// </summary>
        /// <param name="value"></param>
        public Id(int value) : base(value)
        {
        }

        protected override int Prepare(int value) => value;
    }
}