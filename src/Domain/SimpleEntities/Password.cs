﻿namespace Domain.SimpleEntities
{
    /// <summary>
    ///   Пароль.
    /// </summary>
    public sealed class Password : SimpleEntity<string>
    {
        /// <summary>
        /// Создать пароль.
        /// </summary>
        /// <param name="value"> Значение. </param>
        public Password(string value) : base(value)
        {
        }

        protected override string Prepare(string value) => value;
    }
}