﻿namespace Domain.SimpleEntities
{
    using Domain.Exceptions;

    /// <summary>
    /// Наименование объекта с возможным нулевым значением.
    /// </summary>
    public sealed class NameNullable : SimpleEntity<string>
    {
        /// <summary>
        /// Флаг пустого значения.
        /// </summary>
        public bool IsEmpty { get; }
        /// <summary>
        ///   Создать наименование.
        /// </summary>
        /// <param name="value"> Значение. </param>
        public NameNullable(string value) : base(value)
        {
            IsEmpty = value is null;
        }

        protected override string Prepare(string value)
        {
            if (string.IsNullOrWhiteSpace(value))
                return null;

            value = value.Trim();

            return string.IsNullOrWhiteSpace(value)
                ? throw new NameInvalidException()
                : value;
        }
    }
}