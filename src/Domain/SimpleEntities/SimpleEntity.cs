﻿namespace Domain.SimpleEntities
{
    /// <summary>
    ///   Объект-значение.
    /// </summary>
    /// <typeparam name="T"> Тип значения. </typeparam>
    public abstract class SimpleEntity<T>
    {
        /// <summary>
        ///   Значение.
        /// </summary>
        protected readonly T Value;

        /// <summary>
        ///   Создать.
        /// </summary>
        /// <param name="value"> Значение. </param>
        // ReSharper disable once VirtualMemberCallInConstructor
        protected SimpleEntity(T value) => Value = Prepare(value);

        /// <summary>
        ///   Подготовить значение.
        /// </summary>
        /// <param name="value"> Значение. </param>
        /// <returns> Подготовленное значение. </returns>
        protected abstract T Prepare(T value);

        public static implicit operator T(SimpleEntity<T> entity) => entity.Value;
    }
}