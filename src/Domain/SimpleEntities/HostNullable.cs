﻿namespace Domain.SimpleEntities
{
    using System;

    using Domain.Exceptions;

    /// <summary>
    /// Хост с возможным нулевым значением.
    /// </summary>
    public sealed class HostNullable : SimpleEntity<string>
    {
        /// <summary>
        /// Флаг пустого значения.
        /// </summary>
        public bool IsEmpty { get; }
        /// <summary>
        /// Создать хост.
        /// </summary>
        /// <param name="value"> Значение хоста. </param>
        public HostNullable(string value) : base(value)
        {
            IsEmpty = value is null;
        }

        protected override string Prepare(string value)
        {
            if (string.IsNullOrWhiteSpace(value))
                return null;

            return (Uri.CheckHostName(value) == UriHostNameType.Unknown) 
                ? throw new HostInvalidException(value) 
                : value;
        }
    }
}