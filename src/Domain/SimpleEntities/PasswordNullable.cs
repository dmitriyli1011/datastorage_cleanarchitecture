﻿namespace Domain.SimpleEntities
{
    /// <summary>
    /// Пароль с возможным нулевым значением.
    /// </summary>
    public sealed class PasswordNullable : SimpleEntity<string>
    {
        /// <summary>
        /// Флаг пустого значения.
        /// </summary>
        public bool IsEmpty { get; }

        /// <summary>
        /// Создать пароль.
        /// </summary>
        /// <param name="value"> Значение. </param>
        public PasswordNullable(string value) : base(value)
        {
            IsEmpty = value is null;
        }

        protected override string Prepare(string value) => 
            string.IsNullOrWhiteSpace(value)
            ? null
            : value;
    }
}