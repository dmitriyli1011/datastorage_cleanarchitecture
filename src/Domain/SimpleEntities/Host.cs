﻿namespace Domain.SimpleEntities
{
    using System;

    using Domain.Exceptions;

    /// <summary>
    /// Хост.
    /// </summary>
    public sealed class Host : SimpleEntity<string>
    {
        /// <summary>
        /// Создать хост.
        /// </summary>
        /// <param name="value"> Значение хоста. </param>
        public Host(string value) : base(value)
        {
        }

        protected override string Prepare(string value) => 
            !string.IsNullOrWhiteSpace(value) &&
                Uri.CheckHostName(value) == UriHostNameType.Unknown
            ? throw new HostInvalidException(value)
            : value;
    }
}