﻿namespace Domain.SimpleEntities
{
    using Domain.Exceptions;

    /// <summary>
    ///   Порт.
    /// </summary>
    public sealed class Port : SimpleEntity<string>
    {
        /// <summary>
        ///   Максимальное значение портов.
        /// </summary>
        private const int MAX = 65535;

        /// <summary>
        ///   Минимальное значение портов.
        /// </summary>
        private const int MIN = 49152;

        /// <summary>
        ///   Создать порт.
        /// </summary>
        /// <param name="port"> Значение порта. </param>
        public Port(string port) : base(port)
        {
        }

        protected override string Prepare(string port) =>
            int.TryParse(port, out var value)
                ? value >= MIN && value <= MAX
                    ? port
                    : throw new PortInvalidException(port)
                : throw new PortInvalidException(port);
    }
}