﻿namespace Domain.SimpleEntities
{
    using Domain.Exceptions;

    /// <summary>
    ///   Наименование объекта.
    /// </summary>
    public sealed class Name : SimpleEntity<string>
    {
        /// <summary>
        ///   Создать наименование.
        /// </summary>
        /// <param name="value"> Значение. </param>
        public Name(string value) : base(value)
        {
        }

        protected override string Prepare(string value)
        {
            value = value.Trim();

            return string.IsNullOrWhiteSpace(value)
                ? throw new NameInvalidException()
                : value;
        }
    }
}