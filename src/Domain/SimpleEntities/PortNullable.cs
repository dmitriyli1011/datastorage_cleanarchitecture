﻿namespace Domain.SimpleEntities
{
    using Domain.Exceptions;

    /// <summary>
    /// Порт с возможным нулевым значением.
    /// </summary>
    public sealed class PortNullable : SimpleEntity<string>
    {
        /// <summary>
        /// Флаг пустого значения.
        /// </summary>
        public bool IsEmpty { get; }

        /// <summary>
        ///   Максимальное значение портов.
        /// </summary>
        private const int MAX = 65535;

        /// <summary>
        ///   Минимальное значение портов.
        /// </summary>
        private const int MIN = 49152;

        /// <summary>
        /// Создать порт.
        /// </summary>
        /// <param name="port"> Значение порта. </param>
        public PortNullable(string port) : base(port)
        {
            IsEmpty = port is null;
        }

        protected override string Prepare(string port)
        {
            if (string.IsNullOrWhiteSpace(port))
                return null;

            if (int.TryParse(port, out var value) && value >= MIN && value <= MAX)
                return port;
              
            throw new PortInvalidException(port);
        }
    }
}