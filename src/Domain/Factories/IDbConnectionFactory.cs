﻿namespace Domain.Factories
{
    using Domain.Entities;
    using Domain.SimpleEntities;

    /// <summary>
    ///   Фабрика для создания подключений к БД.
    /// </summary>
    public interface IDbConnectionFactory
    {
        /// <summary>
        ///   Создать подключение к БД.
        /// </summary>
        /// <param name="host"> Хост. </param>
        /// <param name="name"> Наименование. </param>
        /// <param name="password"> Пароль. </param>
        /// <param name="port"> Порт. </param>
        /// <returns> Подключение к БД. </returns>
        DbConnection CreateDbConnection(Host host,
            Name name,
            Password password,
            Port port);
    }
}