﻿namespace Domain.Entities
{
    using System;

    /// <summary>
    ///     Поключение к БД.
    /// </summary>
    public abstract class DbConnection
    {
        /// <summary>
        /// Идентификатор.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        ///     Имя соединения.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        ///     Хост.
        /// </summary>
        public string Host { get; set; }

        /// <summary>
        ///     Порт.
        /// </summary>
        public string Port { get; set; }

        /// <summary>
        ///     Пароль
        /// </summary>
        public string Password { get; set; }

        /// <summary>
        ///     Дата создания.
        /// </summary>
        public DateTime CreationDate { get; set; }
    }
}