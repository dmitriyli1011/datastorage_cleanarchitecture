﻿namespace Domain.Repositories
{
    using System.Collections.Generic;
    using System.Threading.Tasks;

    using Domain.Entities;

    /// <summary>
    ///   Репозиторий подключений к БД.
    /// </summary>
    public interface IDbConnectionRepository
    {
        /// <summary>
        /// Создание строки подключения
        /// </summary>
        /// <param name="dbConnection"></param>
        /// <returns></returns>
        Task AddAsync(DbConnection dbConnection);

        /// <summary>
        /// Удаление по уникальному имени строки подключения.
        /// </summary>
        /// <param name="dbConnections"> Список строк подключения. </param>
        /// <returns></returns>
        Task DeleteAsync(IEnumerable<DbConnection> dbConnections);

        /// <summary>
        /// Получение строк подключения по именам.
        /// </summary>
        /// <param name="names"> Имена строк подключения. </param>
        /// <returns> Список строк подключения. </returns>
        Task<IEnumerable<DbConnection>> GetAsync(params int[] ids);

        /// <summary>
        /// Обновление строки подключения.
        /// </summary>
        /// <param name="dbConnection"> Строка подключения. </param>
        /// <returns> Задача. </returns>
        Task UpdateAsync(DbConnection dbConnection);

        /// <summary>
        /// Получить строку подключения по идентификатору.
        /// </summary>
        /// <param name="id"> Идентификатор строки подклчючения. </param>
        /// <returns> Строка подключения. </returns>
        Task<DbConnection> GetAsync(int id);
    }
}