﻿namespace Domain.Exceptions
{
    /// <summary>
    /// Некорректно наименование.
    /// </summary>
    public class NameInvalidException : BaseException
    {
        public NameInvalidException(string value) : base($"Наименование задано некорректно - {value}.")
        {
        }

        public NameInvalidException() : base("Наименование не задано.")
        {
        }
    }
}