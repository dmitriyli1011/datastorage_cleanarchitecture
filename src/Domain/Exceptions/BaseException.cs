﻿namespace Domain.Exceptions
{
    using System;
    using System.Runtime.Serialization;

    /// <summary>
    ///   Базовое исключение приложения.
    /// </summary>
    [Serializable]
    public abstract class BaseException : Exception
    {
        /// <summary>
        ///   Создать исключение.
        /// </summary>
        protected BaseException()
        {
        }

        /// <summary>
        ///   Создать исключение.
        /// </summary>
        /// <param name="message"> Текст ошибки. </param>
        protected BaseException(string message) : base(message)
        {
        }

        /// <summary>
        ///   Создать исключение.
        /// </summary>
        /// <param name="message"> Текст ошибки. </param>
        /// <param name="inner"> Внутреннее исключение. </param>
        protected BaseException(string message, Exception inner) : base(message, inner)
        {
        }

        /// <inheritdoc cref="Exception" />
        protected BaseException(
            SerializationInfo info,
            StreamingContext context) : base(info, context)
        {
        }
    }
}