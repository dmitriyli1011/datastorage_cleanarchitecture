﻿namespace Domain.Exceptions
{
    /// <summary>
    ///   Некорректное значение хоста.
    /// </summary>
    public class HostInvalidException : BaseException
    {
        public HostInvalidException(string host) : base($"Некорректно задано значение хоста - {host}.")
        {
        }
    }
}