﻿namespace Domain.Exceptions
{
    /// <summary>
    /// Некорректный порт.
    /// </summary>
    public class PortInvalidException : BaseException
    {
        public PortInvalidException(string port) : base($"Некорректное значение порта - {port}.")
        {
        }
    }
}