﻿using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace Infrastructure.EntityFramework.Migrations
{
    public partial class AddIntIdInDbConnection : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_DbConnections",
                table: "DbConnections");

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "DbConnections",
                type: "text",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "text");

            migrationBuilder.AddColumn<int>(
                name: "Id",
                table: "DbConnections",
                type: "integer",
                nullable: false,
                defaultValue: 0)
                .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn);

            migrationBuilder.AddPrimaryKey(
                name: "PK_DbConnections",
                table: "DbConnections",
                column: "Id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_DbConnections",
                table: "DbConnections");

            migrationBuilder.DropColumn(
                name: "Id",
                table: "DbConnections");

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "DbConnections",
                type: "text",
                nullable: false,
                defaultValue: "",
                oldClrType: typeof(string),
                oldType: "text",
                oldNullable: true);

            migrationBuilder.AddPrimaryKey(
                name: "PK_DbConnections",
                table: "DbConnections",
                column: "Name");
        }
    }
}
