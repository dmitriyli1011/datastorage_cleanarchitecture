﻿namespace Infrastructure.EntityFramework
{
    using System;

    using Domain.Entities;
    using Domain.Factories;
    using Domain.SimpleEntities;

    using Infrastructure.EntityFramework.Entities;

    /// <summary>
    ///   Фабрика создания сущностей.
    /// </summary>
    internal class EfEntityFactory : IDbConnectionFactory
    {
        public DbConnection CreateDbConnection(Host host, Name name, Password password, Port port) => new EfDbConnection
        {
            Host = host,
            Password = password,
            Name = name,
            CreationDate = DateTime.Now,
            Port = port
        };
    }
}