﻿namespace Infrastructure.EntityFramework.DataAccess
{
    using Infrastructure.EntityFramework.Entities;

    using Microsoft.EntityFrameworkCore;

    /// <summary>
    ///   Контекст БД.
    /// </summary>
    internal class EfContext : DbContext
    {
        /// <summary>
        /// Конструктор.
        /// </summary>
        /// <param name="options"></param>
        public EfContext(DbContextOptions options) : base(options)
        {
        }

        /// <summary>
        /// Таблица строк подключения.
        /// </summary>
        public DbSet<EfDbConnection> DbConnections { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfigurationsFromAssembly(typeof(EfContext).Assembly);
            base.OnModelCreating(modelBuilder);
        }
    }
}