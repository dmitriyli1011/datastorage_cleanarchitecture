﻿namespace Infrastructure.EntityFramework.DataAccess
{
    using System.Threading.Tasks;

    using Application.DataAccess;

    using Microsoft.EntityFrameworkCore;

    /// <inheritdoc cref="IMigrator" />
    internal class EfMigrator : IMigrator
    {
        /// <summary>
        ///   Контекст БД.
        /// </summary>
        private readonly EfContext _context;

        public EfMigrator(EfContext context) => _context = context;

        public Task Migrate() => _context.Database.MigrateAsync();
    }
}