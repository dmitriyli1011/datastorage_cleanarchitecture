﻿namespace Infrastructure.EntityFramework.DataAccess
{
    using System.Threading.Tasks;

    using Application.DataAccess;

    using Domain.Repositories;

    using Infrastructure.EntityFramework.Entities;
    using Infrastructure.EntityFramework.Repositories;

    /// <inheritdoc />
    internal class EfUnitOfWork : IUnitOfWork
    {
        private readonly EfContext _context;

        public EfUnitOfWork(EfContext context)
        {
            _context = context;

            Connections = new EfDbConnectionRepository(_context.Set<EfDbConnection>());
        }

        public ValueTask DisposeAsync() => _context.DisposeAsync();

        public Task<int> Commit() => _context.SaveChangesAsync();

        public IDbConnectionRepository Connections { get; }
    }
}