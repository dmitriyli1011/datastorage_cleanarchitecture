﻿namespace Infrastructure.EntityFramework.Repositories
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    using Domain.Entities;
    using Domain.Repositories;

    using Infrastructure.EntityFramework.Entities;

    using Microsoft.EntityFrameworkCore;

    /// <inheritdoc />
    internal class EfDbConnectionRepository : IDbConnectionRepository
    {
        private readonly DbSet<EfDbConnection> _set;

        public EfDbConnectionRepository(DbSet<EfDbConnection> set)
        {
            _set = set;
        }

        /// <inheritdoc />
        public async Task AddAsync(DbConnection dbConnection)
        {
            await _set.AddAsync((EfDbConnection) dbConnection);
        }

        /// <inheritdoc />
        public Task DeleteAsync(IEnumerable<DbConnection> dbConnections)
        {
            _set.RemoveRange(dbConnections.Cast<EfDbConnection>());
            return Task.CompletedTask;
        }

        /// <inheritdoc />
        public async Task<IEnumerable<DbConnection>> GetAsync(params int[] ids)
        {
            return  await _set.Where(x =>
                ids.Contains(x.Id)).ToListAsync();
        }

        /// <inheritdoc />
        public async Task<DbConnection> GetAsync(int id)
        {
            return await _set.FindAsync(id);
        }

        public Task UpdateAsync(DbConnection dbConnection)
        {
            _set.Update((EfDbConnection)dbConnection);
            return Task.CompletedTask;
        }
    }
}