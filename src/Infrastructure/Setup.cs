﻿namespace Infrastructure
{
    using Application.DataAccess;

    using Domain.Factories;

    using Infrastructure.EntityFramework;
    using Infrastructure.EntityFramework.DataAccess;

    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.DependencyInjection;

    /// <summary>
    ///   Настройка.
    /// </summary>
    public static class Setup
    {
        /// <summary>
        ///   Использовать Postgres.
        /// </summary>
        /// <param name="services"> Коллекция служб. </param>
        /// <param name="connectionString"> Строка подключения. </param>
        /// <returns> Коллекция служб. </returns>
        public static IServiceCollection UsePostgres(this IServiceCollection services, string connectionString) =>
            services
                .AddDbContext<EfContext>(builder =>
                    builder.UseNpgsql(connectionString))
                .AddTransient<IMigrator, EfMigrator>()
                .AddTransient<IUnitOfWork, EfUnitOfWork>()
                .AddTransient<IDbConnectionFactory, EfEntityFactory>();
    }
}