﻿namespace WebApi.Settings
{
    using Application.Boundaries.CreateDbConnection;
    using Application.Boundaries.DeleteDbConnection;
    using Application.Boundaries.ReadDbConnection;
    using Application.Boundaries.UpdateDbConnection;
    using Application.UseCases;

    using Microsoft.Extensions.DependencyInjection;

    using WebApi.UseCases.CreateDbConnection;
    using WebApi.UseCases.DeleteDbConnection;
    using WebApi.UseCases.ReadDBConnection;
    using WebApi.UseCases.UpdateDbConnection;

    /// <summary>
    ///   Расширение настройки сервисов для портов.
    /// </summary>
    public static class Register
    {
        /// <summary>
        ///   Насройка объекта и порта ответа.
        /// </summary>
        public static IServiceCollection UseApi(this IServiceCollection collection) =>
            collection
                .AddScopedUseCaseAndOutputPort<
                    ICreateDbConnectionUseCase,
                    CreateDbConnectionUseCase,
                    ICreateDbConnectionOutputPort,
                    CreateDbConnectionViewModel>()

                .AddScopedUseCaseAndOutputPort<
                    IReadDbConnectionUseCase,
                    ReadDbConnectionUseCase,
                    IReadDbConnectionOutputPort,
                    ReadDbConnectionViewModel>()

                .AddScopedUseCaseAndOutputPort<
                    IUpdateDbConnectionUseCase,
                    UpdateDbConnectionUseCase,
                    IUpdateDbConnectionOutputPort,
                    UpdateDbConnectionVieModel>()

                .AddScopedUseCaseAndOutputPort<
                    IDeleteDbConnectionUseCase,
                    DeleteDbConnectionUseCase,
                    IDeleteDbConnectionOutputPort,
                    DeleteDbConnectionViewModel>();
            
        /// <summary>
        /// Метод регистрации сценария использования и выходного результата.
        /// </summary>
        /// <typeparam name="TIUseCase"> Интерфейс сценария использования. </typeparam>
        /// <typeparam name="TUseCase"> Реализация сценария использования. </typeparam>
        /// <typeparam name="TIOutputPort"> Интерфейс выходного резкльтата. </typeparam>
        /// <typeparam name="TOutputPort"> РЕализация выходного результата. </typeparam>
        /// <param name="collection"> Коллекция сервисов. </param>
        /// <returns> Коллекция сервисов. </returns>
        public static IServiceCollection AddScopedUseCaseAndOutputPort<TIUseCase, TUseCase, TIOutputPort, TOutputPort>(
            this IServiceCollection collection)
            where TIOutputPort : class
            where TOutputPort : class, TIOutputPort
            where TIUseCase : class
            where TUseCase : class, TIUseCase
            =>
                collection
                    .AddScoped<TOutputPort>()
                    .AddScoped<TIOutputPort>(provider =>
                        provider.GetRequiredService<TOutputPort>())
                    .AddScoped<TIUseCase, TUseCase>();
    }
}