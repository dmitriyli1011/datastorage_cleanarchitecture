﻿namespace WebApi.Core
{
    using Microsoft.AspNetCore.Mvc;

    /// <summary>
    ///   Базовый для приложения контроллер.
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    // TODO: рассмотреть свой вариант.
    [ApiConventionType(typeof(DefaultApiConventions))]
    public class AbstractController : ControllerBase
    {
    }
}