﻿namespace WebApi.UseCases.UpdateDbConnection
{
    using System.Threading.Tasks;

    using Application.Boundaries.UpdateDbConnection;

    using Domain.SimpleEntities;

    using Microsoft.AspNetCore.Mvc;

    using WebApi.Core;
    using WebApi.UseCases.CreateDbConnection;

    /// <summary>
    /// Api обновления строки подключения.
    /// </summary>
    public class UpdateDbConnectionController : AbstractController
    {
        [HttpPut]
        public async Task<ActionResult> Put(
            [FromServices] IUpdateDbConnectionUseCase useCase,
            [FromServices] UpdateDbConnectionVieModel output,
            [FromForm] CreateDbConnectionRequest request,
            int id)
        {
            var dbConnection = new UpdateDbConnectionInput
            (
                new Id(id),
                new Host(request.Host),
                new Name(request.Name),
                new Password(request.Password),
                new Port(request.Port)
            );
            await useCase.ExecuteAsync(dbConnection);
            return output.ViewModel;
        }
    }
}