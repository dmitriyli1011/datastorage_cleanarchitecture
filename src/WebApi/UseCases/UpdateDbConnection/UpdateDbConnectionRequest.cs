﻿namespace WebApi.UseCases.UpdateDbConnection
{
    using System.ComponentModel.DataAnnotations;

    public class UpdateDbConnectionRequest
    {
        /// <summary>
        /// Наименование строки подключения.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        ///     Хост.
        /// </summary>
        public string Host { get; set; }

        /// <summary>
        ///     Пароль
        /// </summary>
        public string Password { get; set; }

        /// <summary>
        ///     Порт.
        /// </summary>
        public string Port { get; set; }
    }
}