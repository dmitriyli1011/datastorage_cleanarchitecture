﻿namespace WebApi.UseCases.UpdateDbConnection
{
    using Application.Boundaries.UpdateDbConnection;

    using Domain.Entities;

    using Microsoft.AspNetCore.Http;
    using Microsoft.AspNetCore.Mvc;

    /// <inheritdoc />
    public class UpdateDbConnectionVieModel : IUpdateDbConnectionOutputPort
    {
        public ActionResult ViewModel;
        public void Standard(DbConnection input)
        {
            ViewModel = new OkObjectResult(input)
            {
                StatusCode = StatusCodes.Status200OK
            };
        }

        public void NotFound(string message)
        {
            ViewModel = new ObjectResult(message)
            {
                StatusCode = StatusCodes.Status404NotFound
            };
        }

        public void WriteError(string message)
        {
            ViewModel = new ObjectResult(message)
            {
                StatusCode = StatusCodes.Status500InternalServerError
            };
        }
    }
}