﻿namespace WebApi.UseCases.ReadDBConnection
{
    using System.Threading.Tasks;

    using Application.Boundaries.ReadDbConnection;

    using Microsoft.AspNetCore.Mvc;

    using WebApi.Core;

    /// <summary>
    /// Api получения строки получения.
    /// </summary>
    public class ReadDbConnectionController : AbstractController
    {
        [HttpGet]
        public async Task<ActionResult> Get(
            [FromServices] IReadDbConnectionUseCase useCase,
            [FromServices] ReadDbConnectionViewModel outputPort,
            [FromQuery] int[] request)
        {
            await useCase.ExecuteAsync(request);
            return outputPort.ViewModel;
        }
    }
}