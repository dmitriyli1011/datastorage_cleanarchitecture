﻿namespace WebApi.UseCases.ReadDBConnection
{
    using System.Collections.Generic;

    using Application.Boundaries.ReadDbConnection;

    using Domain.Entities;

    using Microsoft.AspNetCore.Http;
    using Microsoft.AspNetCore.Mvc;

    ///<inheritdoc />
    public class ReadDbConnectionViewModel : IReadDbConnectionOutputPort
    {
        public ActionResult ViewModel;

        public void WriteError(string message)
        {
            ViewModel = new ObjectResult(message)
            {
                StatusCode = StatusCodes.Status500InternalServerError
            };
        }

        public void NotFound(string message)
        {
            ViewModel = new ObjectResult(message)
            {
                StatusCode = StatusCodes.Status404NotFound
            };
        }

        public void Standard(List<DbConnection> input)
        {
            ViewModel = new OkObjectResult(input)
            {
                StatusCode = StatusCodes.Status200OK
            };
        }
    }
}