﻿namespace WebApi.UseCases.CreateDbConnection
{
    using Application.Boundaries.CreateDbConnection;

    using Microsoft.AspNetCore.Http;
    using Microsoft.AspNetCore.Mvc;

    /// <inheritdoc />
    public sealed class CreateDbConnectionViewModel : ICreateDbConnectionOutputPort
    {
        public ActionResult ViewModel;

        public void Standard()
        {
            ViewModel = new OkResult();
        }

        public void WriteError(string message)
        {
            ViewModel = new ObjectResult(message)
            {
                StatusCode = StatusCodes.Status500InternalServerError
            };
        }
    }
}