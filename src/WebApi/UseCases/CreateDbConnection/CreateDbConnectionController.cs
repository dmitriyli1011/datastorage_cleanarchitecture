﻿namespace WebApi.UseCases.CreateDbConnection
{
    using System.Threading.Tasks;

    using Application.Boundaries.CreateDbConnection;

    using Domain.SimpleEntities;

    using Microsoft.AspNetCore.Mvc;

    using WebApi.Core;

    /// <summary>
    ///   Api создания строки подключения.
    /// </summary>
    public class CreateDbConnectionController : AbstractController
    {
        /// <summary>
        ///   Создание строки подключения.
        /// </summary>
        /// <param name="useCase"> Сценарий создания. </param>
        /// <param name="outputPort"> Порт передачи ответа. </param>
        /// <param name="request"> Входящий запрос. </param>
        /// <returns> Статус код. </returns>
        [HttpPost]
        public async Task<ActionResult> Post(
            [FromServices] ICreateDbConnectionUseCase useCase,
            [FromServices] CreateDbConnectionViewModel outputPort,
            [FromBody] CreateDbConnectionRequest request
        )
        {
            var input = new CreateDbConnectionInput
            (
                new Host(request.Host),
                new Name(request.Name),
                new Password(request.Password),
                new Port(request.Port)
            );

            await useCase.ExecuteAsync(input);

            return outputPort.ViewModel;
        }
    }
}