﻿namespace WebApi.UseCases.CreateDbConnection
{
    using System.ComponentModel.DataAnnotations;

    /// <summary>
    /// Запрос на создание подключения к БД.
    /// </summary>
    public sealed class CreateDbConnectionRequest
    {
        /// <summary>
        ///     Хост.
        /// </summary>
        [Required]
        public string Host { get; set; }

        /// <summary>
        ///     Имя соединения.
        /// </summary>
        [Required]
        public string Name { get; set; }

        /// <summary>
        ///     Пароль
        /// </summary>
        [Required]
        public string Password { get; set; }

        /// <summary>
        ///     Порт.
        /// </summary>
        [Required]
        public string Port { get; set; }
    }
}