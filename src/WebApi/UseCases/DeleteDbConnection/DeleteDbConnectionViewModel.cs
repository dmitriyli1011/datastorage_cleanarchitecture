﻿namespace WebApi.UseCases.DeleteDbConnection
{
    using Application.Boundaries.DeleteDbConnection;

    using Microsoft.AspNetCore.Http;
    using Microsoft.AspNetCore.Mvc;

    ///<inheritdoc cref="IDeleteDbConnectionOutputPort"/>
    public class DeleteDbConnectionViewModel : IDeleteDbConnectionOutputPort
    {
        public ActionResult ViewModel;
        public void Standard()
        {
            ViewModel = new OkResult();
        }

        public void WriteError(string message)
        {
            ViewModel = new ObjectResult(message)
            {
                StatusCode = StatusCodes.Status500InternalServerError
            };
        }

        public void NotFound(string message)
        {
            ViewModel = new ObjectResult(message)
            {
                StatusCode = StatusCodes.Status404NotFound
            };
        }
    }
}