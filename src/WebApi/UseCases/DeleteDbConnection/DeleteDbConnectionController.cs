﻿namespace WebApi.UseCases.DeleteDbConnection
{
    using System.Threading.Tasks;

    using Application.Boundaries.DeleteDbConnection;

    using Microsoft.AspNetCore.Mvc;

    using WebApi.Core;
    /// <summary>
    ///   Api удаления строки подключения.
    /// </summary>
    public class DeleteDbConnectionController : AbstractController
    {
        [HttpDelete]
        public async Task<ActionResult> Delete(
            [FromServices] IDeleteDbConnectionUseCase useCase,
            [FromServices] DeleteDbConnectionViewModel output,
            int[] ids)
        {
            await useCase.ExecuteAsync(ids);
            return output.ViewModel;
        }
    }
}