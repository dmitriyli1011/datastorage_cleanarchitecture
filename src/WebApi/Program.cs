namespace WebApi
{
    using System.Threading.Tasks;

    using Application.DataAccess;

    using Microsoft.AspNetCore.Hosting;
    using Microsoft.Extensions.DependencyInjection;
    using Microsoft.Extensions.Hosting;

    public class Program
    {
        public static IHostBuilder CreateHostBuilder(string[] args)
        {
            return Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder => webBuilder.UseStartup<Startup>());
        }

        public static async Task Main(string[] args)
        {
            var host = CreateHostBuilder(args).Build();

            await Migrate(host);

            host.Run();
        }

        /// <summary>
        ///   ������������� ������.
        /// </summary>
        /// <param name="host"> ����. </param>
        /// <returns> ������. </returns>
        private static async Task Migrate(IHost host)
        {
            using var scope = host.Services.CreateScope();
            var migrator = scope.ServiceProvider.GetRequiredService<IMigrator>();

            await migrator.Migrate();
        }
    }
}