﻿namespace Application.Boundaries.CreateDbConnection
{
    using Application.UseCases;

    /// <summary>
    ///   Создание строки подключения.
    /// </summary>
    public interface ICreateDbConnectionUseCase : IUseCase<CreateDbConnectionInput>
    {
    }
}