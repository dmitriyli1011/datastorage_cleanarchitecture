﻿namespace Application.Boundaries.CreateDbConnection
{
    using Domain.SimpleEntities;

    /// <summary>
    ///   Входное сообщение для создания подключения к БД.
    /// </summary>
    public class CreateDbConnectionInput
    {
        public CreateDbConnectionInput(
            Host host,
            Name name,
            Password password,
            Port port)
        {
            Host = host;
            Name = name;
            Password = password;
            Port = port;
        }

        /// <summary>
        /// Хост.
        /// </summary>
        internal Host Host { get; }

        /// <summary>
        /// Наименование.
        /// </summary>
        internal Name Name { get; }

        /// <summary>
        /// Пароль.
        /// </summary>
        internal Password Password { get; }

        /// <summary>
        /// Порт.
        /// </summary>
        internal Port Port { get; }
    }
}