﻿namespace Application.Boundaries.CreateDbConnection
{
    using Application.Boundaries;

    /// <summary>
    /// Выходное сообщение для создания подключения к БД.
    /// </summary>
    public interface ICreateDbConnectionOutputPort : IOutputPortStandard, IOutputPortError
    {
        
    }
}