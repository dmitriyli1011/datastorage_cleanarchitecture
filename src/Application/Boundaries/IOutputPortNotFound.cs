﻿namespace Application.Boundaries
{
    /// <summary>
    ///   Не найдено.
    /// </summary>
    public interface IOutputPortNotFound
    {
        /// <summary>
        ///   Сообщить, что результат не найден.
        /// </summary>
        /// <param name="message"> Описание. </param>
        void NotFound(string message);
    }
}