﻿namespace Application.Boundaries
{
    /// <summary>
    ///   Стандартный выходной результат.
    /// </summary>
    public interface IOutputPortStandard
    {
        /// <summary>
        ///   Записать данные.
        /// </summary>
        void Standard();
    }

    /// <summary>
    ///   Стандартный выходной результат.
    /// </summary>
    /// <typeparam name="TUseCaseInput"> Тип входных данных. </typeparam>
    public interface IOutputPortStandard<in TUseCaseInput>
    {
        /// <summary>
        ///   Записать данные.
        /// </summary>
        /// <param name="input"> Данные. </param>
        void Standard(TUseCaseInput input);
    }
}