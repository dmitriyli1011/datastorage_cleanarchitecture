﻿namespace Application.Boundaries.ReadDbConnection
{
    using System.Collections.Generic;

    using Domain.Entities;

    /// <summary>
    ///  Выходное сообщение при получении строк подключения.
    /// </summary>
    public interface IReadDbConnectionOutputPort : IOutputPortStandard<List<DbConnection>>, IOutputPortError, IOutputPortNotFound
    {
        
    }
}