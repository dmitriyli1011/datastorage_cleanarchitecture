﻿namespace Application.Boundaries.ReadDbConnection
{
    using Application.Boundaries.CreateDbConnection;
    using Application.UseCases;
    
    /// <summary>
    /// Получение строки подключения.
    /// </summary>
    public interface IReadDbConnectionUseCase : IUseCase<int[]>
    {
    }
}