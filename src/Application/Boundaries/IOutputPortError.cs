﻿namespace Application.Boundaries
{
    /// <summary>
    ///   Произошла ошибка.
    /// </summary>
    public interface IOutputPortError
    {
        /// <summary>
        ///   Сообщить об ошибке.
        /// </summary>
        /// <param name="message"> Описание ошибки. </param>
        void WriteError(string message);
    }
}