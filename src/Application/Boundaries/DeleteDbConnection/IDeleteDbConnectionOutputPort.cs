﻿namespace Application.Boundaries.DeleteDbConnection
{
    /// <summary>
    /// Выходное сообщение при удалении строки подключения.
    /// </summary>
    public interface IDeleteDbConnectionOutputPort : IOutputPortStandard, IOutputPortError, IOutputPortNotFound
    {
    }
}