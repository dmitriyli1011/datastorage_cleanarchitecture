﻿namespace Application.Boundaries.DeleteDbConnection
{
    using Application.UseCases;

    /// <summary>
    /// Удаление строки подключения.
    /// </summary>
    public interface IDeleteDbConnectionUseCase : IUseCase<int[]>
    {
    }
}