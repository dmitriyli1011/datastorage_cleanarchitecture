﻿namespace Application.Boundaries.UpdateDbConnection
{
    using Domain.SimpleEntities;

    /// <summary>
    ///   Входное сообщение для обновления подключения к БД.
    /// </summary>
    public class UpdateDbConnectionInput
    {
        public UpdateDbConnectionInput(
            Id id,
            Host host,
            Name name,
            Password password,
            Port port)
        {
            Id = id;
            Host = host;
            Name = name;
            Password = password;
            Port = port;
        }

        internal Id Id { get; }

        /// <summary>
        /// Хост.
        /// </summary>
        internal Host Host { get; }

        /// <summary>
        /// Наименование.
        /// </summary>
        internal Name Name { get; }

        /// <summary>
        /// Пароль.
        /// </summary>
        internal Password Password { get; }

        /// <summary>
        /// Порт.
        /// </summary>
        internal Port Port { get; }
    }
}