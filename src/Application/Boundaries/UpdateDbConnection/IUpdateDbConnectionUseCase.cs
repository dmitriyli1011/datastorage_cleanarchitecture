﻿namespace Application.Boundaries.UpdateDbConnection
{
    using Application.Boundaries.CreateDbConnection;
    using Application.UseCases;

    /// <summary>
    /// Обновление строки подключения.
    /// </summary>
    public interface IUpdateDbConnectionUseCase : IUseCase<UpdateDbConnectionInput>
    {
    }
}