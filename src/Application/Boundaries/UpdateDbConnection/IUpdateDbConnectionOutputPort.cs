﻿namespace Application.Boundaries.UpdateDbConnection
{
    using Domain.Entities;

    /// <summary>
    /// Выходное сообщение для обовления подключения к БД.
    /// </summary>
    public interface IUpdateDbConnectionOutputPort : 
        IOutputPortStandard<DbConnection>, 
        IOutputPortNotFound, 
        IOutputPortError
    {
    }
}