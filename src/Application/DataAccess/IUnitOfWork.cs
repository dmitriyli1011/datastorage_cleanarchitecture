﻿namespace Application.DataAccess
{
    using Domain.Repositories;
    using System;
    using System.Threading.Tasks;

    /// <summary>
    ///   Единица работы.
    /// </summary>
    public interface IUnitOfWork : IAsyncDisposable
    {
        /// <summary>
        ///   Подключения к БД.
        /// </summary>
        IDbConnectionRepository Connections { get; }

        /// <summary>
        ///   Выполнить.
        /// </summary>
        Task<int> Commit();
    }
}