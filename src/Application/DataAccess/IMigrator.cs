﻿namespace Application.DataAccess
{
    using System.Threading.Tasks;

    /// <summary>
    ///   Мигратор БД.
    /// </summary>
    public interface IMigrator
    {
        /// <summary>
        ///   Мигрировать.
        /// </summary>
        /// <returns> Задача. </returns>
        Task Migrate();
    }
}