﻿namespace Application.UseCases
{
    using System.Linq;
    using System.Threading.Tasks;

    using Application.Boundaries.ReadDbConnection;
    using Application.DataAccess;

    ///<inheritdoc />
    public class ReadDbConnectionUseCase : IReadDbConnectionUseCase
    {
        ///<inheritdoc cref="IUnitOfWork"/>
        private readonly IUnitOfWork _unitOfWork;

        ///<inheritdoc cref="IReadDbConnectionOutputPort"/>
        private readonly IReadDbConnectionOutputPort _outputPort;

        public ReadDbConnectionUseCase(IUnitOfWork unitOfWork, IReadDbConnectionOutputPort outputPort)
        {
            _unitOfWork = unitOfWork;
            _outputPort = outputPort;
        }

        public async Task ExecuteAsync(int[] input)
        {
            try
            {
                var dbConnection = await _unitOfWork.Connections.GetAsync(input);

                if (dbConnection is null)
                {
                    _outputPort.NotFound("Не найдены строки подключния с заданными именами.");
                    return;
                }

                _outputPort.Standard(dbConnection.ToList());
            }
            catch
            {
                _outputPort.WriteError("Ошибка при получении данных.");
            }
        }
    }
}