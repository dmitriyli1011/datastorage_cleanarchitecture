﻿namespace Application.UseCases
{
    using System;
    using System.Threading.Tasks;

    using Application.Boundaries.CreateDbConnection;
    using Application.Boundaries.DeleteDbConnection;
    using Application.DataAccess;

    using Domain.Exceptions;

    ///<inheritdoc />
    public class DeleteDbConnectionUseCase : IDeleteDbConnectionUseCase
    {
        ///<inheritdoc cref="IUnitOfWork"/>
        private readonly IUnitOfWork _unitOfWork;

        ///<inheritdoc cref="IDeleteDbConnectionOutputPort"/>
        private readonly IDeleteDbConnectionOutputPort _output;

        public DeleteDbConnectionUseCase(
            IUnitOfWork unitOfWork,
            IDeleteDbConnectionOutputPort output)
        {
            _unitOfWork = unitOfWork;
            _output = output;
        }

        public async Task ExecuteAsync(int[] input)
        {
            try
            {
                if (input is null)
                {
                    _output.WriteError("Не переданы имена строк для удаления.");
                    return;
                }

                var dbConnections = await _unitOfWork.Connections.GetAsync(input);
                if (dbConnections is null)
                {
                    _output.NotFound("Не найдены строки подлючения с заданными именами.");
                    return;
                }
                    
                await _unitOfWork.Connections.DeleteAsync(dbConnections);
                await _unitOfWork.Commit();
                _output.Standard();
            }
            catch (Exception)
            {
                _output.WriteError("Ошибка при удалении.");
            }
        }
    }
}