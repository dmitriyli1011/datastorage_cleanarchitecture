﻿namespace Application.UseCases
{
    using System;
    using System.Linq;
    using System.Threading.Tasks;

    using Application.Boundaries.UpdateDbConnection;
    using Application.DataAccess;

    using Domain.Factories;

    ///<inheritdoc />
    public class UpdateDbConnectionUseCase : IUpdateDbConnectionUseCase
    {
        private readonly IDbConnectionFactory _factory;
        private readonly IUpdateDbConnectionOutputPort _output;
        private readonly IUnitOfWork _unitOfWork;

        public UpdateDbConnectionUseCase(IUnitOfWork unitOfWork, IUpdateDbConnectionOutputPort output,
            IDbConnectionFactory factory)
        {
            _unitOfWork = unitOfWork;
            _output = output;
            _factory = factory;
        }

        public async Task ExecuteAsync(UpdateDbConnectionInput input)
        {
            try
            {
                var dbConnection = await _unitOfWork.Connections.GetAsync(input.Id);

                if (dbConnection is null)
                {
                    _output.NotFound("Не найдены строки подлючения с заданными именами.");
                    return;
                }

                dbConnection.Name = input.Name;
                dbConnection.Host = input.Host;
                dbConnection.Port = input.Port;
                dbConnection.Password = input.Password;

                await _unitOfWork.Connections.UpdateAsync(dbConnection);
                await _unitOfWork.Commit();
                _output.Standard(dbConnection);
            }
            catch (Exception)
            {
                _output.WriteError("Ошибка в обновлении строки подключения");
            }
        }
    }
}