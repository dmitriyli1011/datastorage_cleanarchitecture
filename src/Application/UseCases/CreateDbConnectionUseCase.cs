﻿namespace Application.UseCases
{
    using System.Threading.Tasks;

    using Application.Boundaries.CreateDbConnection;
    using Application.DataAccess;

    using Domain.Factories;

    ///<inheritdoc cref="ICreateDbConnectionUseCase"/>
    /// TODO: логирование, asserts.
    public class CreateDbConnectionUseCase : ICreateDbConnectionUseCase
    {
        private readonly ICreateDbConnectionOutputPort _output;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IDbConnectionFactory _factory;

        public CreateDbConnectionUseCase(IUnitOfWork unitOfWork, ICreateDbConnectionOutputPort output, IDbConnectionFactory factory)
        {
            _output = output;
            _unitOfWork = unitOfWork;
            _factory = factory;
        }

        public async Task ExecuteAsync(CreateDbConnectionInput input)
        {
            var connection = _factory.CreateDbConnection(input.Host, input.Name, input.Password, input.Port);

            try
            {
                await _unitOfWork.Connections.AddAsync(connection);
                await _unitOfWork.Commit();
                _output.Standard();
            }
            catch
            {
                _output.WriteError("Не удалось создать подключение к БД.");
            }
        }
    }
}