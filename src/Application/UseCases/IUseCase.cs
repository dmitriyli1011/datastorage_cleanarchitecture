﻿namespace Application.UseCases
{
    using System.Threading.Tasks;

    /// <summary>
    ///   Сценарий использования.
    /// </summary>
    /// <typeparam name="TUseCaseInput"> Тип входных данных. </typeparam>
    public interface IUseCase<in TUseCaseInput>
    {
        /// <summary>
        ///   Исполнить.
        /// </summary>
        /// <param name="input"> Входные данные. </param>
        /// <returns> Задача. </returns>
        Task ExecuteAsync(TUseCaseInput input);
    }
}